import dash_html_components as html
import dash_core_components as dcc

def Header():
    return html.Div([
        get_logo(),
        get_header(),
        html.Br([]),
        get_menu()
    ])

def get_logo():
    logo = html.Div([
        html.Div([
            html.Img(src='assets/logo.png', height='48', width='48')
        ], className="padded")
    ], className="gs-header")
    return logo


def get_header():
    header = html.Div([
        html.Div([
            html.H5(
                'COVID-19 Pandemic')
        ], className="padded")
    ], className="gs-header gs-text-header")
    return header


def get_menu():
    menu = html.Div([

        dcc.Link('Overview   ', href='/overview', className="tab first"),

        dcc.Link('Country details   ', href='/country', className="tab"),

        dcc.Link('News   ', href='/news', className="tab")

    ])
    return menu