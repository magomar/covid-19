from typing import List

import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

import data.jhu_data as data_src
from . import Header
from .plots import *
from .print_button import print_button

keys = ['confirmed', 'dead', 'recovered']
daily = [f'daily_{k}' for k in keys]


def build_dashboard(app, num_countries=10, start_date=None):
    data = data_src.get_all_data(start_date=start_date)
    all_countries = data_src.get_countries_sorted(start_date=start_date).index
    countries = all_countries[:num_countries]

    noPage = html.Div([  # 404
        html.P(["404 Page not found"])
    ], className="no-page")

    # Describe the layout, or the UI, of the app
    app.layout = html.Div([
        dcc.Location(id='url', refresh=False),
        html.Div(id='page-content')
    ])

    overview = get_overview(data, countries)

    @app.callback(Output('page-content', 'children'),
                  [Input('url', 'pathname')])
    def display_page(pathname):
        if pathname == '/' or pathname == '/overview':
            return overview
        else:
            return noPage


def get_overview(data: pd.DataFrame, countries: List[str]):
    return html.Div([  # page 1
        print_button(),
        Header(),
        html.Div([
            html.H3("Overview of the Covid-19 evolution in the World"),
            get_accumulated_cases(data, countries),
            get_daily_cases(data, countries)
        ])
    ])


def get_country(data: pd.DataFrame, countries: List[str]):
    return html.Div([
        print_button(),
        dcc.Dropdown(
            id='selected_country',
            options=[{'label': c, 'value': c} for c in countries],
            value=countries[0]
        ),
    ])


def get_accumulated_cases(data: pd.DataFrame, countries: List[str]):
    return html.Div(
        [dcc.Graph(
            id=f'{k}_by_country',
            figure=line_plot(data, k, countries)) for k in keys]
    )


def get_daily_cases(data: pd.DataFrame, countries):
    return html.Div(
        [dcc.Graph(
            id=f'{k}_by_country',
            figure=bar_plot(data, k, countries)) for k in daily])
