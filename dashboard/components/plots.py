from typing import Iterable

import pandas as pd
import plotly.graph_objs as go


def line_plot(data: pd.DataFrame, key: str, countries: Iterable[str]):
    traces = []

    for country in countries:
        country_data = data[data.country == country]
        traces.append(go.Scatter(
            x=country_data.date,
            y=country_data[key],
            mode='lines+markers',
            name=country
        ))

    layout = go.Layout(
        title=f'Evolution of {key} cases'
    )
    fig = go.Figure(data=traces, layout=layout)
    return fig


def bar_plot(data: pd.DataFrame, key: str, countries: Iterable[str]):
    traces = []

    for country in countries:
        country_data = data[data.country == country]
        traces.append(go.Bar(
            x=country_data.date,
            y=country_data[key],
            name=country
        ))

    layout = go.Layout(
        title=f'Evolution of {key} cases'
    )
    fig = go.Figure(data=traces, layout=layout)
    return fig
