import matplotlib.pyplot as plt

from .jhu_data import *


def main():
    countries = get_countries_sorted()
    data = get_all_data()
    plt.figure(figsize=(16, 10))
    for country in countries.index[:10]:
        plt.plot('date', 'confirmed', data=data[data.country == country], label=country)
    plt.legend()
    plt.show()


if __name__ == '__main__':
    main()
