import sys

import dash

from components import build_dashboard

if __name__ == '__main__':
    args = sys.argv
    start_date = args[1] if len(args) > 0 else None
    app = dash.Dash(__name__)
    app.config.suppress_callback_exceptions = True
    build_dashboard(app, start_date=start_date)

    # external_css = ["https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css",
    #                 "https://cdnjs.cloudflare.com/ajax/libs/skeleton/2.0.4/skeleton.min.css",
    #                 "//fonts.googleapis.com/css?family=Raleway:400,300,600",
    #                 "https://codepen.io/bcd/pen/KQrXdb.css",
    #                 "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"]
    # # https://codepen.io/chriddyp/pen/bWLwgP.css
    # for css in external_css:
    #     app.css.append_css({"external_url": css})
    #
    # external_js = ["https://code.jquery.com/jquery-3.2.1.min.js",
    #                "https://codepen.io/bcd/pen/YaXojL.js"]
    #
    # for js in external_js:
    #     app.scripts.append_script({"external_url": js})

    app.run_server(debug=True)
